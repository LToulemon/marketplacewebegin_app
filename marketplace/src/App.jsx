import Product from "./pages/Product";
import Home from "./pages/Home";
import ProductList from "./pages/ProductList";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Cart from "./pages/Cart";

import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom"
const App = () => {
  const user = true
  return (
    <BrowserRouter>
      <Routes>
          <Route path='/' element={<Home/>}></Route>
          <Route path="/products" element={<ProductList/>}></Route>
          <Route path="/product/:id" element={<Product/>}></Route>
          <Route path="/product/:category" element={<Product/>}></Route>
          <Route path="/cart" element={<Cart/>}></Route>
          <Route path="/login" element={<Login/>}></Route> 
          <Route path="/register" element={<Register/>}></Route>
      </Routes>
    </BrowserRouter>
  );
};

export default App;